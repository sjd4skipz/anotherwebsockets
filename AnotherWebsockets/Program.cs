﻿using System;
using WebSocket4Net;

namespace AnotherWebsockets
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Client client = new Client ();
			client.Setup("ws://10.10.10.254:7681/root/");
			client.Start();
		}
	}
}
